#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista(){}

/* Función para añadir un dato a la lista */
void Lista::add_nodo(){
    int var = 0;

    Nodo *directriz = new Nodo;

    cout << "Ingrese una nueva variable a la lista: ";    
    cin >> var;

    registrar_cnt(var);

    directriz->dato = var; 

    directriz->next = NULL;

    /* Función para verificar si es el primero */
    if (this->first_data == NULL) { 
        this->first_data = directriz;
        this->last_data = this->first_data;

    } else {
        
        Nodo *indice, *pre_indice;

        indice = first_data;

        while((indice != NULL) && (indice->dato < directriz->dato)){
            pre_indice = indice;
            indice = indice->next;
        }

        if(indice == first_data){
            first_data = directriz;
            first_data->next = indice;
        
        }else{
            pre_indice->next = directriz;
            directriz->next = indice;
        }
    }
    cout << "Se ha guardado el " << directriz->dato << " en la lista." << endl << endl;
}

/* Función para registrar el dato mayor o menor ingresado */
void Lista::registrar_cnt(int cnt){
    if (this->cnt_mayor < cnt){
        this->cnt_mayor = cnt;
    }
    if (this->cnt_menor > cnt){
        this->cnt_menor = cnt;
    }
}


/* Función para mostrar todos los nodos */
void Lista::mostrar_lista(){
    cout << "Variables almacenadas en la lista: " << endl;

    Nodo *indice = new Nodo();

    indice = first_data;

    if(first_data != NULL){
        while(indice != NULL){
            cout << " " << indice->dato;
            indice = indice->next;
        }
        cout << endl;

    }else{
        cout << "Lista sin variables. " << endl;   
    }
    cout << endl;  
}

/* Función para rellenar la lista con valores ordenados desde el menor a mayor dato ingresado */
void Lista::rellenar_lista(){

    Nodo *indice = new Nodo();

    indice = first_data;

    if(first_data != NULL){
        while(indice != NULL){
            for (int i = this->cnt_menor; i < this->cnt_mayor+1; i++){
                if(indice->dato != i){

                    Nodo *directriz = new Nodo;

                    directriz->dato = i;

                    directriz->next = NULL;

                    Nodo *nodo_dato, *pre_nodo_dato;

                    nodo_dato = first_data;

                    while((nodo_dato != NULL) && (nodo_dato->dato < directriz->dato)){
                        pre_nodo_dato = nodo_dato;
                        nodo_dato = nodo_dato->next;
                    }

                    if(nodo_dato == first_data){
                        first_data = directriz;
                        first_data->next = nodo_dato;
                        
                    }else{
                        pre_nodo_dato->next = directriz;
                        directriz->next = nodo_dato;
                    }

                    cout << "Se ha guardado el " << directriz->dato << " en la lista." << endl;
                }else{
                    indice = indice->next;
                }
            }
        }  
    }else{
        cout << "Lista sin variables. " << endl;   
    }
    cout << endl;  
}