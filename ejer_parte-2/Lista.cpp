#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista(){}

/* Función para añadir un dato a la lista */
void Lista::add_nodo(){

    Nodo *directriz = new Nodo;

    cout << "Ingrese una nueva variable a la lista: ";    
    cin >> directriz->dato;

    directriz->next = NULL;

    /* Función para verificar si es el primero */
    if (this->first_data == NULL) { 
        this->first_data = directriz;
        this->last_data = this->first_data;

    } else {
        
        Nodo *indice, *pre_indice;

        indice = first_data;

        while((indice != NULL) && (indice->dato < directriz->dato)){
            pre_indice = indice;
            indice = indice->next;
        }

        if(indice == first_data){
            first_data = directriz;
            first_data->next = indice;
        
        }else{
            pre_indice->next = directriz;
            directriz->next = indice;
        }
    }
    cout << "Se ha guardado el " << directriz->dato << " en la lista." << endl << endl; 
}

/* Función para mostrar todos los nodos */
void Lista::mostrar_lista(int num_lista){
    cout << "Variables almacenadas en la lista " << num_lista << ": " << endl;

    Nodo *indice = new Nodo();

    indice = first_data;

    if(first_data != NULL){
        while(indice != NULL){
            cout << " " << indice->dato;
            indice = indice->next;
        }
        cout << endl << endl;

    }else{
        cout << "Lista sin variables. " << endl << endl;   
    }
}

/* Función sumar las variables de una lista a otra */
void Lista::sumar_lista(Lista *sum_nodo){

    Nodo *tmp = new Nodo();
    tmp = sum_nodo->first_data;

    while(tmp!= NULL){

        Nodo *directriz = new Nodo;

        directriz->dato = tmp->dato;

        directriz->next = NULL;

        if (this->first_data == NULL) { 
            this->first_data = directriz;
            this->last_data = this->first_data;

        } else {
            
            Nodo *indice, *pre_indice;

            indice = first_data;

            while((indice != NULL) && (indice->dato < directriz->dato)){
                pre_indice = indice;
                indice = indice->next;
            }

            if(indice == first_data){
                first_data = directriz;
                first_data->next = indice;
            
            }else{
                pre_indice->next = directriz;
                directriz->next = indice;
            }
        }
        cout << "Se ha guardado el " << directriz->dato << " en la lista." << endl;   

        tmp = tmp->next;
    }
}