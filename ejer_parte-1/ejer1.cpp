#include<iostream>
using namespace std;

#include "Lista.h"

/* Función para limpiar la terminal */
void clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función para el menu principal */
string menu_principal (string opt) {
    cout << "========= Menu =========" << endl;
    cout << "Agregar nodo  ______ [1]" << endl;
    cout << "Mostrar lista  _____ [2]" << endl;
    cout << "Salir del programa _ [0]" << endl;
    cout << "========================" << endl;
    cout << "Opción: ";

    cin >> opt;
    
    clear();

    return opt;
}

int main(){
    string option = "\0";

    Lista *lista = new Lista();

    while (option != "0") {

        option = menu_principal(option);

        if (option == "1") {
            lista->add_nodo();
        }

        if (option == "2") {
            lista->mostrar_lista();
        } 
    }
    return 0;
}
