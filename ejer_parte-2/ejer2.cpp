#include<iostream>
using namespace std;

#include "Lista.h"


/* Función para limpiar la terminal */
void clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función para el menu principal */
string menu_principal (string opt) {
    cout << "============= Menu =============" << endl;
    cout << "Agregar nodo (Lista 1) _____ [1]" << endl;
    cout << "Mostrar conjunto (Lista 1) _ [2]" << endl << endl;

    cout << "Agregar nodo (Lista 2) _____ [3]" << endl;
    cout << "Mostrar conjunto (Lista 2) _ [4]" << endl << endl;

    /* Generar nodos: se generan las variables a partir de la lista 1 y lista 2 */
    cout << "Generar nodos (Lista 3) ____ [5]" << endl;
    cout << "Mostrar conjunto (Lista 3) _ [6]" << endl << endl;

    cout << "Mostrar todos los conjuntos  [7]" << endl << endl;

    cout << "Salir del programa _________ [0]" << endl;
    cout << "================================" << endl;
    cout << "Opción: ";

    cin >> opt;
    
    clear();

    return opt;
}

int main(){
    string option = "\0";

    Lista *lista_1 = new Lista();
    Lista *lista_2 = new Lista();
    Lista *lista_3 = new Lista();

    while (option != "0") {

        option = menu_principal(option);

        if (option == "1") {
            lista_1->add_nodo();

        }else if (option == "2") {
            lista_1->mostrar_lista(1);

        }else if (option == "3") {
            lista_2->add_nodo();

        }else if (option == "4") {
            lista_2->mostrar_lista(2);

        }else if (option == "5") {
            lista_3->sumar_lista(lista_1);
            lista_3->sumar_lista(lista_2);

        }else if (option == "6") {
            lista_3->mostrar_lista(3);

        }else if (option == "7") {
            lista_1->mostrar_lista(1);
            lista_2->mostrar_lista(2);
            lista_3->mostrar_lista(3);

        }
    }
    return 0;
}