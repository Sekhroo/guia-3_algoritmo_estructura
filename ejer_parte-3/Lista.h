#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;

typedef struct _Nodo {
    int dato;
    struct _Nodo *next;
} Nodo;

class Lista {

    private:
        Nodo *first_data = NULL;
        Nodo *last_data = NULL;
        int cnt_mayor = -100000000;
        int cnt_menor = 100000000;

    public:
        Lista();

        void add_nodo();

        void registrar_cnt(int cnt);

        void mostrar_lista();

        void rellenar_lista();

};
#endif